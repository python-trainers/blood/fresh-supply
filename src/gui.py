from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.injections import CodeInjectionUI
from trainerbase.gui.misc import SeparatorUI
from trainerbase.gui.speedhack import SpeedHackUI

from injections import god_mode, infinite_ammo, one_hit_kill


@simple_trainerbase_menu("Blood™ Fresh Supply", 640, 290)
def run_menu():
    add_components(
        CodeInjectionUI(god_mode, "God Mode", "F9"),
        CodeInjectionUI(infinite_ammo, "Infinite Ammo", "F10"),
        CodeInjectionUI(one_hit_kill, "One Hit Kill", "F11"),
        SeparatorUI(),
        SpeedHackUI(),
    )
