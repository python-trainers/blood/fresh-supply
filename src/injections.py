from trainerbase.codeinjection import AllocatingCodeInjection
from trainerbase.memory import pm


# "anuket_x64.exe" + 0x1EC816
# AOB: 48 8B 87 30 01 00 00 66 83 3C 58 00 0F 8E 79 01 00 00
god_mode = AllocatingCodeInjection(
    pm.base_address + 0x1EC816,
    """
        mov rax, [rdi + 0x130]

        mov word [rax + rbx * 2], 9999

        cmp word [rax + rbx * 2], 0x0
    """,
    original_code_length=12,
)

# "anuket_x64.exe" + 0x1EE3E6
# AOB: 66 B1 A0 E3 FF 48 8B 83 E8 01 00 00 48 69 CF B0 00 00 00 + 0xA
infinite_ammo = AllocatingCodeInjection(
    pm.base_address + 0x1EE3E6,
    """
        imul rcx, rdi, 0xB0
        mov rcx, [rcx + rax + 0x60]

        mov byte [rcx], 9
    """,
    original_code_length=12,
)

# "anuket_x64.exe" + 0x15A19D
# AOB: 89 47 48 48 8B 8B 38 01 00 00 48 85 C9 74 0E
one_hit_kill = AllocatingCodeInjection(
    pm.base_address + 0x15A19D,
    """
        test eax, eax
        jz skip
        cmp dword [rdi + 0x48], 0
        je skip
        xor eax, eax

        skip:

        mov [rdi + 0x48], eax
        mov rcx, [rbx + 0x138]
        test rcx, rcx
    """,
    original_code_length=13,
)
